<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);

    //Sections
    $wp_customize->add_section('sage_copyrigth', [
        'title' => __('Copyright', 'sage'),
        'priority' => 30
    ]);
    
    //Settings
    $wp_customize->add_setting( 'sage_copyrigth', array(
        'default' => '',
        'transport' => 'refresh',
    ) );

    //Controls
    $wp_customize->add_control( 'sage_copyrigth', array(
        'label' => __( 'Copyright Text', 'sage' ),
        'section' => 'sage_copyrigth',
        'type' => 'text',
    ) );
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
