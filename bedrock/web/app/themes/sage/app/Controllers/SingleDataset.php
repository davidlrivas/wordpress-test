<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleDataset extends Controller
{
    protected $acf = true;

    public function date()
    {
        return get_field('fecha');
    }
}