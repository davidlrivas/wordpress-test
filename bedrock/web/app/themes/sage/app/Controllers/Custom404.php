<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Custom404 extends Controller
{
    protected $template = '404';

    public function searchPerson()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.noencontrado.org/v1/search/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        return $response->data;
    }

    public function dataSet()
    {
        $post_items = get_posts(array(
            'post_type' => 'dataset',
            'posts_per_page' => 4,
            'orderby' => 'title',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'slug',
                    'terms' => 'administracion'
                )
            )
        ));

        return array_map(function ($post) {
            return [
                'title' => $post->post_title,
                'link' => get_permalink($post->ID),
                'excerpt' => wp_trim_words($post->post_content, 20),
                'date' => get_field('fecha', $post->ID)
            ];
        }, $post_items);
    }
}