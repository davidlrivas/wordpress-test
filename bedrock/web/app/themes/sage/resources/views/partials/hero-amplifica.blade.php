<div class="px-4 py-5 my-5">
    <h1 class="display-5 fw-bold text-center">Amplifica Challenge</h1>
    <div class="col-lg-6 mx-auto">
        <p class="lead mb-4 text-center">Para la realización de este challenge se cubrieron las siguientes tareas.<br></p>
        <ul class="list-group">
            <li class="list-group-item">Instalación de Bedrock y Sage 9</li>
            <li class="list-group-item">Creación de una página 404 custom.</li>
            <li class="list-group-item">Utilización de API Pública "No Encontrado" y creación de card para mostrar el contenido (Imágen, nombre, localidad y residencia).</li>
            <li class="list-group-item">Creación de un Custom Post Type y un Custom Field de tipo Fecha asociado al mismo.</li>
            <li class="list-group-item">Creación de una card para los ultimos 4 posts de la categoría "Administración" del Custom Post Type "Dataset" y botón para ver más.</li>
            <li class="list-group-item">Utilización de un template de Header y Footer de Bootstrap.</li>
            <li class="list-group-item">Instalación de ACF Local JSON para guardar los ACF localmente.</li>
        </ul>
        <hr class="separator">
        <p class="lead mb-4 text-center">Sugerencias.<br></p>
        <ul class="list-group">
            <li class="list-group-item">Agregar una imágen de la sección "Datasets" a modo de ejemplo</li>
            <li class="list-group-item">Problemas con LocalWP para utilizar la última versión de Sage dado que se requiere una versión más actualizada de PHP y
            problemas para utilizar Trellis en Macs M1 dado que VirtualBox o VMWare no funcionan. 
            Una Solución podría ser entregar una imágen de Docker para la realización del Challenge.</li>
        </ul>
        <!-- <label for="basic-url" class="form-label">Genere una URL inexistente</label>
        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon3">{{ get_bloginfo('url') }}</span>
            <input type="text" class="form-control" id="custom-url-text" aria-describedby="basic-addon3">
        </div>
        <div id="view-custom-404" onclick="redirectTo404()" class="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <a href="#" class="btn btn-primary btn-lg px-4 gap-3">Ver página 404</a>
        </div> -->
    </div>
</div>