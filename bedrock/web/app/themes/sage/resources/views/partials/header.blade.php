
  <div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
      <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
        <span class="fs-4">{{ get_bloginfo('name', 'display') }}</span>
      </a> 
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills']) !!}
      @else
      <ul class="nav nav-pills">
        <li class="nav-item"><a href="{{ get_bloginfo('url') }}" class="nav-link">Inicio</a></li>
      </ul>
      @endif
    </header>
  </div>


