<div id="modulo-1" class="px-4 py-5 my-5 text-center">
  <p class="lead">Módulo 1</p>
  <h1 class="display-5 fw-bold">Ayudanos a encontrarlos</h1>
  <div class="card mb-12" style="max-width: 540px;">
  <div class="row g-0">
    <div class="col-md-4">
      <img src="http://static.noencontrado.org/img/{{ $search_person->id }}.jpg" class="img-fluid rounded-start" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">{{ $search_person->nombre }}</h5>
        <p class="card-text">Fecha:<small class="text-muted"> {{ $search_person->fecha_desaparicion }}</small></p>
        <p class="card-text">Localidad:<small class="text-muted"> {{ $search_person->residencia }}</small></p>
      </div>
    </div>
  </div>
</div>