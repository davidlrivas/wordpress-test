<div id="modulo-2" class="container">
    <p class="lead">Módulo 2</p>
    <h1 class="display-5 fw-bold">Dataset</h1>
    @foreach ($data_set as $post)
    <div class="card card-dataset">
        <div class="card-body">
        <h5 class="card-title">{!! $post['title'] !!}</h5>
        <p class="card-date">{!! $post['date'] !!}</p>
        <p class="card-text">{!! $post['excerpt'] !!}</p>
        <a href="{!! $post['link'] !!}" class="btn btn-primary">Ver post</a>
        </div>
    </div>
    @endforeach
</div>