<div class="container">
  <footer class="py-3 my-4">
    @if(has_nav_menu('footer_navigation'))
      {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav justify-content-center border-bottom pb-3 mb-3']) !!}
    @else
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="{{ get_bloginfo('url') }}" class="nav-link px-2 text-muted">Inicio</a></li>
    </ul>
    @endif
    <p class="text-center text-muted">Amplifica Challenge.</p>
  </footer>
</div>

