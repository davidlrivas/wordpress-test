@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-'.get_post_type())

    @if($images)
      <ul>
        @foreach($images as $image)
          <li><img src="{{$image['sizes']['thumbnail']}}" alt="{{$image['alt']}}"></li>
        @endforeach
      </ul>
    @endif
  @endwhile
@endsection
