@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="p-5 mb-4 bg-light rounded-3">
    <div class="container-fluid py-5">
      <h1 class="display-5 fw-bold">{{ the_title() }}</h1>
      @if($date)
      <p class="col-md-8 fs-4">Date: {{$date}}</p>
      @endif
      <p class="col-md-8 fs-4">{{ the_content() }}</p>
    </div>
  </div>
  @endwhile
@endsection