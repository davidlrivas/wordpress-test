@extends('layouts.app')

@section('content')
  @if (!have_posts())
    @include('partials.card-search')
    @if (!empty($data_set))
    <hr class="separator">
    @include('partials.card-dataset')
    @endif
  @endif
@endsection
